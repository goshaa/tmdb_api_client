<?php

namespace Goshaa\Tmbd;

class TmdbApiClient
{
    protected $apiUrl = "https://api.themoviedb.org/3";
    protected $tmdbUrl = "https://www.themoviedb.org";
    protected $ch;
    protected $apiKey;
    protected $lang;
    
    public function __construct($apiKey, $lang)
    {
        $this->apiKey = $apiKey;
        $this->lang = $lang;
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
    }
    
    public function getResults($count)
    {
        $results = [];
        $page = 1;
    
        while (count($results) < $count)
        {
            curl_setopt($this->ch, CURLOPT_URL, "{$this->apiUrl}/movie/top_rated?api_key={$this->apiKey}&language={$this->lang}&page={$page}");
            $output = curl_exec($this->ch);
            $results = array_merge($results, json_decode($output)->results);
            $page++;
        }
        return array_slice($results, 0, $count);
    }
    
    public function getDetails($movieId)
    {
        curl_setopt($this->ch, CURLOPT_URL, "{$this->apiUrl}/movie/{$movieId}?api_key={$this->apiKey}&language={$this->lang}");
        $output = curl_exec($this->ch);
    
        return json_decode($output);
    }
    
    public function getGenres()
    {
        curl_setopt($this->ch, CURLOPT_URL, "{$this->apiUrl}/genre/movie/list?api_key={$this->apiKey}&language={$this->lang}");
        $output = curl_exec($this->ch);
    
        return json_decode($output)->genres;
    }
    
    public function getCredits($movieId)
    {
        curl_setopt($this->ch, CURLOPT_URL, "{$this->apiUrl}/movie/{$movieId}/credits?api_key={$this->apiKey}&language={$this->lang}");
        $output = curl_exec($this->ch);
    
        return json_decode($output);
    }
    
    public function getPerson($personId)
    {
        curl_setopt($this->ch, CURLOPT_URL, "{$this->apiUrl}/person/{$personId}?api_key={$this->apiKey}&language={$this->lang}");
        $output = curl_exec($this->ch);
        
        return json_decode($output);
    }
    
    public function getTmdbUrl($movieId)
    {
        return $this->tmdbUrl.'/movie/'.$movieId;
    }
    
    public function getPosterUrl($path)
    {
        return $this->tmdbUrl."/t/p/w300_and_h450_bestv2/".$path;
    }
    
    public function __destruct()
    {
        curl_close($this->ch);
    }
}
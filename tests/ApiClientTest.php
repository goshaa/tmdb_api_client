<?php

use Goshaa\Tmbd\TmdbApiClient;
use PHPUnit\Framework\TestCase;

class ApiClientTest extends TestCase
{
    protected $client;
    
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->client = new TmdbApiClient('d6831dd10c567589b5b7841ae7a35a26', 'en-US');
    }
    
    public function testGetResults()
    {
        $this->assertTrue(is_array($this->client->getResults(25)));
        $this->assertEquals(25, count($this->client->getResults(25)));
        $this->assertTrue(is_int($this->client->getResults(25)[0]->id));
    }
    
    public function testGetDetails()
    {
        $id = $this->client->getResults(25)[0]->id;
        $this->assertTrue(is_object($this->client->getDetails($id)));
        $this->assertTrue(is_int($this->client->getDetails($id)->id));
        $this->assertTrue(is_string($this->client->getDetails($id)->title));
    }
    
    public function testGetGenres()
    {
        $this->assertTrue(is_array($this->client->getGenres()));
        $this->assertTrue(is_int($this->client->getGenres()[0]->id));
        $this->assertTrue(is_string($this->client->getGenres()[0]->name));
    }
    
    public function testGetCredits()
    {
        $id = $this->client->getResults(25)[0]->id;
        $this->assertTrue(is_object($this->client->getCredits($id)));
        $this->assertTrue(is_array($this->client->getCredits($id)->crew));
        $this->assertTrue(is_string($this->client->getCredits($id)->crew[0]->job));
    }
    
    public function testGetTmdbUrl()
    {
        $id = $this->client->getResults(25)[0]->id;
        $this->assertTrue(is_string($this->client->getTmdbUrl($id)));
        $this->assertTrue(strpos($this->client->getTmdbUrl($id), 'http') > -1);
        $this->assertTrue(strpos($this->client->getTmdbUrl($id), '/movie/') > -1);
    }
    
    public function testGetPosterUrl()
    {
        $id = $this->client->getResults(25)[0]->id;
        $path = $this->client->getDetails($id)->poster_path;
        $this->assertTrue(is_string($this->client->getPosterUrl($path)));
        $this->assertTrue(strpos($this->client->getPosterUrl($path), 'http') > -1);
        $this->assertTrue(strpos($this->client->getPosterUrl($path), '/t/p/w300_and_h450_bestv2/'.$path) > -1);
    }
}